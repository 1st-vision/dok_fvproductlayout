.. |label| replace:: Tabellarische Produktliste
.. |snippet| replace:: FvProductLayout
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.4.0
.. |maxVersion| replace:: 5.4.6
.. |Version| replace:: 1.0.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis


Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Das Plugin installiert ein weiteres Produkt Layout was im Backend unter Kategorien ausgewählt werden kann. Im Frontend wird dann in der Kategorie eine tabellarische Liste der Produkte ohne Bild und Langtext angezeigt.


Plugin-Einstellungen
--------------------
Folgendes kann eingestellt werden:

.. image:: settings1.png


Frontend
--------
Folgende Spalten werden angezeigt:

- BestellNr. (diese Spalte kann in den Plugin-Einstellungen mit der Artikeldetailseite verlinkt werden)
- Eigenschaften vom Artikel (diese Felder bauen sich automatisch auf; wenn ein Artikel der Kategorie eine bestimmte Eigenschaft besitzt, dann wird die jeweilige Spalte angezeigt)
- Lagerbestand (diese Spalte kann in den Plugin-Einstellungen ausgeblendet werden)
- Listenpreis
- Menge
- Button "Warenkorb"

.. image:: frontend1.png


Backend
-------
In den Artikel-Eigenschaften kann eingestellt werden ob die Eigenschaft im Listing angezeigt werden soll und die jeweilige Bezeichnung in der Tabelle (Tabellenkopft in 2 Zeilen aufgeteilt).
Die Übersetzungen können Sie normal über die Textbausteine hinterlegen.

.. image:: backend1.png


Im Menüpunkt "Artikel > Kategorien" wählt man das neue Produkt-Layout:

.. image:: backend2.png



technische Beschreibung
------------------------

Shop-Datenbank:
_______________
Keine zusätzliche Tabelle vorhanden.


Erweiterung Attribute:
______________________
Die Tabelle "s_filter_options_attributes" wird um folgende Attribute erweitert:

- fv_product_layout_show_in_catlisting
- fv_product_layout_description_row1
- fv_product_layout_description_row2


Modifizierte Template-Dateien
-----------------------------
- listing/index.tpl
- listing/listing.tpl
- listing/listing_ajax.tpl


